#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# shodan_query.py
#
# Búsqueda en SHODAN e imprime los resultados
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.

# https://shodan.readthedocs.io/en/latest/

import shodan
import sys

API_KEY = ''


def main(API_KEY=None):
    if API_KEY == '':
        try:
            import secrets
            API_KEY = secrets.API_KEY
        except Exception:
            print('No hay clave disponible para acceder al API de Shodan')
            exit(2)

    if len(sys.argv) == 1:
        print('Uso: {} <search query>'.format(sys.argv[0]))
        sys.exit(1)

    try:
        api = shodan.Shodan(API_KEY)

        query = ' '.join(sys.argv[1:])
        result = api.search(query)

        campos = ('ip_str', 'product', 'port', 'info', 'isp', 'cpe',
                  'version', 'location.country_name', 'hostnames', 'domains',
                  '_shodan.module')

        # imprime los títulos de las columnas
        print('#' + '\t'.join([c for c in campos]))

        # recorrer cada resultado
        for service in result['matches']:
            for c in campos:
                dato = ''
                registro = service
                for i in c.split('.'):
                    if i in registro:
                        dato = registro[i]
                        registro = dato
                print(dato, end='\t')
            print('')

    except Exception as e:
        print('Error:', e)
        exit(1)

    return 0


if __name__ == '__main__':
    main(API_KEY)
