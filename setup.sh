#!/bin/sh
#
#  setup.sh
#

set -e

SELF='setup.sh'

entorno=venv

if [ ! -e ${entorno}/bin/activate ]; then
    python3 -m virtualenv --python=python3 --always-copy ${entorno}
fi

source ${entorno}/bin/activate

pip install --upgrade -r requirements.txt

echo Puede lanzar las búsquedas en shodan con:
echo
echo python shodan_query.py apache
