
# Shodan search

## Automatización de búsquedas en SHODAN

Para realizar las consultas al servicio Shodan se programó mediante python3 
utilizando la libreria recomendada en la documentación del servicio.

* [Librería de acceso a api de shodan](https://www.github.com/achillean/shodan-python "shodan_python en gitHub")

* [Documentación de la librería](https://shodan.readthedocs.org/ "Documentación")

Para configurar el `API_KEY` de SHODAN, podemos incorporarlo dentro de
`shodan_query.py` o crear un modulo `secrets.py` que contenga la línea

 ```python
API_KEY = 'nuestra_clave_api_rest'
```

Para ejecutar `shodan_query.py` es necesario crear un entorno virtual con:

```
python3 -m venv venv

source venv/bin/activate

```

Instalamos todas las dependencias

```
pip install -r requirements.txt --upgrade

```

Se puede omitir los pasos anteriores, desde la creación del entorno virtual, y
ejecutar el archivo `setup.sh`:

```
bash setup.py

```

Finalmente podemos ejecutarla búsqueda dentro de SHODAN, por ejemplo de
servidores apache:

```
python3 shodan_query.py apache

```

El resultado es una tabla tabulada `[TAB]` que se puede redirigir a un archivo
de este modo:

```
python3 shodan_query.py apache >> resultados.txt

```

## Listado de librerías utilizadas

| Name          | Version  | License    | Author                      |
|---------------|----------|------------|-----------------------------|
| Click         | 7.0      | BSD        | Armin Ronacher              |
| XlsxWriter    | 1.1.5    | BSD        | John McNamara               |
| certifi       | 2019.3.9 | MPL-2.0    | Kenneth Reitz               |
| chardet       | 3.0.4    | LGPL       | Daniel Blanchard            |
| click-plugins | 1.0.4    | New BSD    | Kevin Wurster, Sean Gillies |
| colorama      | 0.4.1    | BSD        | Jonathan Hartley            |
| idna          | 2.8      | BSD-like   | Kim Davies                  |
| requests      | 2.21.0   | Apache 2.0 | Kenneth Reitz               |
| shodan        | 1.11.1   | UNKNOWN    | John Matherly               |
| urllib3       | 1.24.1   | MIT        | Andrey Petrov               |

