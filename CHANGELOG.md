
# Lista de cambios

## v0.3 (Diciembre de 2019)

### Mejoras

- Mejoras en el estilo de código

## v0.2 (Junio de 2019)

### Mejoras

- Mejoras el setup de instalación
- Se agrega licencia utilizada en el desarrollo
- Se listan las librerías utilizadas en la documentación
- Se agrega changelog

## v0.1 (Abril 2019)

### Mejoras

- Se conecta a la API de Shodan por medio de la librería shodan
- Se listan los campos mas interesantes de la respuesta de Shodan
- Se genera una ayuda de instalación vía setup.sh
